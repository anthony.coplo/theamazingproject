import unittest
from app import sum

class Test(unittest.TestCase):

    def test_sum(self):
        value =sum(3,3)
        self.assertEqual(value,6)
    
    def test_sum_null(self):
        value =sum(0,0)
        self.assertEqual(value,0)


if __name__ == '__main__':
    unittest.main()